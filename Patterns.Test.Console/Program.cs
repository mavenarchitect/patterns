﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Builder;

namespace Patterns.Test.Console
{
    class Program
    {

        public static void BuilderInheritanceDemo()
        {
            var me = Person2.New
                .Called("Umair").WorkAs("Manager").Build();
            System.Console.WriteLine(me);

        }

        public static void FacadeBuilderDemo()
        {
            var pb = new PersonBuilder();
            var person = pb
                .Lives.At("81 Agrics Town").In("Lahore").WithPostCode("54000")
                .Works.At("Confiz Limited").AsA("Principal Software Engineer").Earning(5000000);
            System.Console.WriteLine(person);

            Person person2 = pb
                .Lives.At("81 Agrics Town").In("Lahore").WithPostCode("54000")
                .Works.At("Confiz Limited").AsA("Principal Software Engineer").Earning(5000000);
            System.Console.WriteLine(person2);
        }
        static void Main(string[] args)
        {
            LifeWithoutBuilder lifeWithoutBuilder = new LifeWithoutBuilder();
            lifeWithoutBuilder.Run();

            //ordinary non fluent builder
            HtmlBuilder builder = new HtmlBuilder("ul");
            builder.AddChild("li","hello");
            builder.AddChild("li","world");
            System.Console.WriteLine(builder);

            //with fluent builder
            HtmlBuilder ul = new HtmlBuilder("ul");
            ul.AddChildFluent("li", "hello").AddChildFluent("li", "world");
            System.Console.WriteLine(builder);

            //PersonJobBuilder person = new PersonJobBuilder();
            //person.Called("umair").WorkAs("PSE");

            BuilderInheritanceDemo();
            FacadeBuilderDemo();
            System.Console.ReadKey();
        }
    }
}
