﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patterns.Builder
{
    public class Person1
    {
        public string Name { get; set; }
        public string Position { get; set; }

        public override string ToString()
        {
            return $"{nameof(Name)}:{Name}, {nameof(Position)}: {Position}";
        }
    }

    public class PersonInfoBuilder1
    {
        protected Person2 person = new Person2();

        public PersonInfoBuilder1 Called(string name)
        {
            person.Name = name;
            return this;
        }
    }

    public class PersonJobBuilder1 : PersonInfoBuilder1
    {
        public PersonJobBuilder1 WorkAs(string position)
        {
            person.Position = position;
            return this;
        }

    }
}
