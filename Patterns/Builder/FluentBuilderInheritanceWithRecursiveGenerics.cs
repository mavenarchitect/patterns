﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patterns.Builder
{
    public class Person2
    {
        public string Name { get; set; }
        public string Position { get; set; }

        public class Builder2 : PersonJobBuilder2<Builder2>
        {
            
        }

        public static Builder2 New => new Builder2();
        public override string ToString()
        {
            return $"{nameof(Name)}:{Name}, {nameof(Position)}:{Position}";
        }
    }

    public abstract class PersonBuilder2<SELF> 
        where SELF:PersonBuilder2<SELF>
    {
        protected Person2 person = new Person2();

        public Person2 Build()
        {
            return person;
        }
    }

    public class PersonInfoBuilder2<SELF>: PersonBuilder2<PersonInfoBuilder2<SELF>> 
        where SELF: PersonInfoBuilder2<SELF>
    {
        public SELF Called(string name)
        {
            person.Name = name;
            return (SELF) this;
        }
    }

    public class PersonJobBuilder2<SELF>: PersonInfoBuilder2<PersonJobBuilder2<SELF>> 
        where SELF : PersonJobBuilder2<SELF>
    {
        public SELF WorkAs(string position)
        {
            person.Position = position;
            return (SELF) this;
        }

    }
}
