﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patterns.Builder
{
    public class HtmlBuilder
    {
        private string RootName { get; set; }
        public Builder.HtmlElement Root { get; set; }

        public HtmlBuilder(string rootName)
        {
            this.RootName = rootName;
            Root = new Builder.HtmlElement(rootName,string.Empty);
        }

        public void AddChild(string childName, string childText)
        {
            Root.Elements.Add(new Builder.HtmlElement(name:childName,text:childText));
        }

        public HtmlBuilder AddChildFluent(string childName, string childText)
        {
            Root.Elements.Add(new Builder.HtmlElement(name:childName,text:childText));
            return this;
        }

        public override string ToString()
        {
            return Root.ToString();
        }

        public void Clear()
        {
            Root = new Builder.HtmlElement(name:RootName,text:string.Empty);
        }
    }

    public class HtmlElement
    {
        private string Name;
        private string Text;
        public List<HtmlElement> Elements;
        private const int IndentSize = 2;

        public HtmlElement(string name, string text)
        {
            this.Name = name;
            this.Text = text;
            Elements = new List<HtmlElement>();
        }

        private string ToStringImplementation(int indent)
        {
            var stringBuilder = new StringBuilder();
            var i = new string(' ', IndentSize * indent);
            stringBuilder.Append($"{i}<{Name}>\n");
            if (!string.IsNullOrWhiteSpace(Text))
            {
                stringBuilder.Append(new string(' ', IndentSize * (indent + 1)));
                stringBuilder.Append(Text);
                stringBuilder.Append("\n");
            }

            foreach (var htmlElement in Elements)
            {
                stringBuilder.Append(htmlElement.ToStringImplementation(indent + 1));
            }

            stringBuilder.Append($"{i}</{Name}>\n");
            return stringBuilder.ToString();

        }

        public override string ToString()
        {
            return ToStringImplementation(0);
        }
    }
}
