﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace Patterns.Builder
{
    public class LifeWithoutBuilder
    {
        public void Run()
        {
            //test
            var hello = "hello";
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("<p>");
            stringBuilder.Append(hello);
            stringBuilder.Append("</p>");
            WriteLine(stringBuilder);

            var word = new[] {"hello", "world"};
            stringBuilder.Clear();
            stringBuilder.Append("<ul>");
            foreach (var w in word)
            {
                stringBuilder.AppendFormat("<li>{0}</li>", w);
            }

            stringBuilder.Append("</ul>");

            WriteLine(stringBuilder);
        }
    }
}
