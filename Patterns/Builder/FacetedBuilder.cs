﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Text;

namespace Patterns.Builder
{
    public class Person
    {
        //address
        public string StreetAddress, PostCode, City;

        //employment
        public string CompanyNamne, Position;
        
        //income
        public int AnnualIncome;

        public override string ToString()
        {
            return
                $"{nameof(StreetAddress)}:{StreetAddress}, {nameof(PostCode)}:{PostCode}, {nameof(City)}:{City}, {nameof(CompanyNamne)}:{CompanyNamne}, {nameof(Position)}: {Position}, {nameof(AnnualIncome)}:{AnnualIncome}";
        }
        
    }

    // We need two facet, facet for address, facet for employment
    // PersonBuilder is a facade for other builders
    // Doesn't actually build up a person by itself but it keeps a reference to the person that is build up
    // It also allows us to access sub builders like we need two facet, facet for address, facet for employment
    public class PersonBuilder // This is actually facade
    {
        //we have a reference to object that is being build up 
        // and it initialize here
        protected Person Person = new Person();

        public PersonJobBuilder Works=> new PersonJobBuilder(person:Person);
        public PersonAddressBuilder Lives => new PersonAddressBuilder(person:Person);

        public static implicit operator Person(PersonBuilder pb)
        {
            return pb.Person;
        }

    }

    public class PersonJobBuilder : PersonBuilder
    {
        public PersonJobBuilder(Person person)
        {
            this.Person = person;
        }

        public PersonJobBuilder At(string companyName)
        {
            this.Person.CompanyNamne = companyName;
            return this;
        }

        public PersonJobBuilder AsA(string position)
        {
            this.Person.Position = position;
            return this;
        }

        public PersonJobBuilder Earning(int income)
        {
            this.Person.AnnualIncome = income;
            return this;
        }
    }

    public class PersonAddressBuilder : PersonBuilder
    {
        public PersonAddressBuilder(Person person)
        {
            this.Person = person;
        }

        public PersonAddressBuilder At(string streetAddress)
        {
            this.Person.StreetAddress = streetAddress;
            return this;
        }

        public PersonAddressBuilder In(string city)
        {
            this.Person.City = city;
            return this;
        }

        public PersonAddressBuilder WithPostCode(string postCode)
        {
            this.Person.PostCode = postCode;
            return this;
        }
    }
}
